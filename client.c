// C Chat Client, produced using code found online
// and as suggested by classmates.
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#define PORT 49153


//Main function
int main(int argc, char const *argv[])
{
	struct sockaddr_in address;
	int socket_num = 0, valread;
	struct sockaddr_in server_addr;
	char c;
	char buffer[1024] = {0};
  char username[80];
  char message[100];

//check socket
	if ((socket_num= socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		printf("\n Socket creation error \n");
		return -1;
	}

	memset(&server_addr, '0', sizeof(server_addr));

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORT);

//Identify IP data and convert to nums
	if(inet_pton(AF_INET, "10.115.20.250", &server_addr.sin_addr)<=0)
	{
		printf("\nInvalid address/ Try another address \n");
		return -1;
	}
//push away failed connections
	if (connect(socket_num, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
	{
		printf("\nConnection Failed \n");
		return -1;
  }
//username entry point
  printf("%s\n",buffer );
  valread = read( socket_num, buffer, 1024);
  printf("Enter your username:");
  gets(username);
  printf("%s is connecting. . .  \n", username);
  send(socket_num, username, strlen(username), 0);
//while client connected, message ability
while (1) {

  printf("Enter Message: ");
  		fgets(message, 100, stdin);
  		send(socket_num, message, strlen(message), 0);

  		read(socket_num, buffer, 100);		//reads data from server

  		printf("%s\n", buffer);		//Prints data from the server when pressing enter

          }
  return 0;
}
